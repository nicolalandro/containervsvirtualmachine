# Docker
* [Docker vs Virtualization](https://monkeyvault.net/docker-vs-virtualization/)
* [DevOps Playbook. Pratiche e strumenti](https://medium.com/@fminzoni/devops-playbook-pratiche-e-strumenti-1ce4ca0716a7)
* [DevOps Playbook. Docker e i container applicativi.](https://medium.com/@fminzoni/devops-playbook-docker-e-i-container-applicativi-a0731ec1de3d)
* [Using Docker to support reproducible research](pfigshare-u-files.s3.amazonaws.com/1590657/WSSSPE2_Docker.pdf)
* [An introduction to Docker for reproducible research, with examples from the R environment](https://arxiv.org/pdf/1410.0846.pdf)
* [Docker Hub](https://hub.docker.com/)
* [Considerations for Running Docker for Windows Server 2016 with Hyper-V VMs](https://blog.docker.com/2016/10/considerations-running-docker-windows-server-2016-hyper-v-vms/)
* [Use an ARG in Dockerfile FROM for dynamic image specification](https://www.jeffgeerling.com/blog/2017/use-arg-dockerfile-dynamic-image-specification)
* [Use multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build/)

# Vagrant
* [Vagrant providers: Virtual Box, VMWare or AWS](https://www.vagrantup.com/intro/getting-started/providers.html)
* [How To Provision NGINX Using Vagrant](https://vegibit.com/how-to-provision-nginx-using-vagrant/)
* [Vagrant Cloud](https://app.vagrantup.com/boxes/search)

# Distribute Docker
* [Docker swarm](https://docs.docker.com/engine/swarm/)
* [Docker push to registry](https://docs.docker.com/engine/reference/commandline/push/)
* [Gitlab Container Registry](https://gitlab.com/nicolalandro/ai_block/container_registry)
* [Google Cloud Container Registry](https://cloud.google.com/container-registry/)
* [Azure Container Registry](https://azure.microsoft.com/en-us/services/container-registry/)
* [Create a private local docker registry](https://hackernoon.com/create-a-private-local-docker-registry-5c79ce912620)

# Docker beyond x86 CPU
* [Docker for test raspberry python hello app](https://github.com/nicolalandro/rpi-travis)
* [Nvidia Docker](https://github.com/NVIDIA/nvidia-docker)

# Docker with GUI
* [Docker Containers on the Desktop](https://blog.jessfraz.com/post/docker-containers-on-the-desktop/)

# Scalability
* [Applicazioni Cloud Native. Scalabilità e Microservizi.](https://medium.com/@fminzoni/applicazioni-cloud-native-scalabilit%C3%A0-e-microservizi-ceb37cbd662e)
* [The scaling cube](https://microservices.io/articles/scalecube.html)
* [The scale cube](https://akfpartners.com/growth-blog/scale-cube)

# Docker loging
* [Docker Graylog](https://docs.graylog.org/en/latest/pages/installation/docker.html)
* [Bunyan JSON Logs with Fluentd and Graylog](https://jsblog.insiderattack.net/bunyan-json-logs-with-fluentd-and-graylog-187a23b49540)

# Load Balancing 
* [Load Balancing](https://medium.com/insatiableminds/load-balancing-f894f1e9e9d4)

# Postgres Replica
* [Replicating Postgres inside Docker](https://medium.com/@2hamed/replicating-postgres-inside-docker-the-how-to-3244dc2305be)
* [gosu](https://github.com/tianon/gosu/releases/tag/1.10)

# Consul Replica on demand
* [Applicazioni Cloud Native. Service Discovery.](https://medium.com/@fminzoni/applicazioni-cloud-native-service-discovery-b793ff3c1ca3)
* [Code Consul and Go](https://github.com/LOG-ED/cloud-native-app/tree/discovery)
* [pypi: consul-service-discovery 0.1.2](https://pypi.org/project/consul-service-discovery/)
* [What’s the Difference Between DNS and Service Discovery?](https://medium.com/microscaling-systems/whats-the-difference-between-dns-and-service-discovery-dec4055ce4e2)
