# Vagrant
* first execution time: ./exec.sh  10,76s user 7,81s system 13% cpu 2:13,87 total
* second exec: ./exec.sh  3,37s user 1,92s system 10% cpu 51,921 total
* curl "http://localhost:8080"  0,00s user 0,01s system 68% cpu 0,015 total
* RAM 4.1 GB - 3.7 GB 
* Disk Usage: 1,7 GB

# Docker
* first execution time: 0,07s user 0,02s system 0% cpu 37,106 total
* second exec: ./exec.sh  0,04s user 0,01s system 6% cpu 0,804 total
* curl "http://localhost:8080"  0,01s user 0,00s system 52% cpu 0,011 total
* RAM 448,0 kB
* Disk Usage: 195 MB

