from flask import Flask
import consul
import socket

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/health")
def health():
    return "OK"


if __name__ == '__main__':
    host_name = socket.gethostname()
    ip = socket.gethostbyname(host_name)
    service = consul.Client(endpoint='http://consul:8500')
    service.register(
        id='example-%s' % str(ip),
        name='example',
        address=str(ip),
        port=5000,
        check={
            'Interval': '10s',
            'HTTP': "http://%s:5000/health" % str(ip)
        }
    )

    app.run('0.0.0.0', 5000)
