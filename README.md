# ContainerVsVirtualMachine
Cloud Data Management Exam 

# Projects
## Slide
```
cd Slide
docker-compose up
firefox localhost:8001
# pdf link: http://localhost:8001/?print-pdf#/
# markdown to pdf per bibliografia https://www.markdowntopdf.com/
```
## Usecase: Docker Vs Vagrant
* docker
```
cd UseCase/NginixDockerVsVagrant/docker
time ./exec
docker ps # show the container pid
docker stop pid
docker rm pid
```
* vagrant
```
cd UseCase/NginixDockerVsVagrant/vagrant
time ./exec
vagrant halt
vagrant destroy
```

## Usecase: Docker Registry
```
cd UseCase/DockerRegistry
docker-compose up
cd ./docker_push_test
./deploy_container.sh
firefox localhost:8081 # show the pushed container
```

## Usecase: raspbian
```
cd UseCase/RaspbianContainer
./requirements # install the requirements
./exec.sh # open a shell in a raspbian container
exit
```

## Usecase: Tor Browser with GUI
```
cd UseCase/TorBrowserWithGui
docker-compose up
```

## Usecase: haproxy and nginix
```
cd UseCase/HaProxyAnd3NginX
docker-compose up
firefox localhost:8000
# refresh with F5 and see the different slave response
```

## Usecase: scale Postgres master and slave replicas
```
cd UseCase/PostgresReplica
docker-compose up
firefox localhost:5050 # pgadmin4
# connect to db pg_cluster

# Query SQL
# $ SELECT client_addr, state
# $ FROM pg_stat_replication;
# > see one slave

docker-compose scale pg_slave=2

# Query SQL
# $ SELECT client_addr, state
# $ FROM pg_stat_replication;
# > see two slave
```

## Usecase: scale on demand with Consul
```
cd UseCase/ConsulScaleOnDemand
docker-compose up
firefox localhost:5000 # python app
firefox localhost:8600 # consul
firefox localhost:8080/stats # haproxy
docker-compose scale app=3
firefox localhost:8600 # consul have new subrscription
firefox localhost:8080/stats # haproxy reach new slave
```


## Usecase: envoy with tho service
```
cd UseCase/EnvoyServiceMeshEasySample
docker-compose up
firefox localhost:8000/service/1 # first service
firefox localhost:8000/service/2 # second service
firefox localhost:8001 # envoy interface
```

## Usecase: Graylog Nginix
```
cd UseCase/GrayLogAndNginiX
docker-compose up
firefox localhost:9000 # graylog gui
# Do login with user: admin, password: admin
# go to system -> input
# select input -> gelf udp and go on
docker-compose -f nginix-docker-compose.yml up
firefox localhost:3000 # visit enginx so it logs
# go to graylog -> Search and see the results
```

# Docker Compose commands reminder
* remove all stopped container: ```docker rm $(docker ps -a -q)```
* remove all volume used: ```docker-compose down -v```
* scale: ```docker-compose scale <name>=2```
